﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CicekSatis
{
    public partial class Form2 : Form
    {
        Kullanici giris = null;
        public Form2(Kullanici k)
        {
            InitializeComponent();
            giris = k;
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            List<VwKategori> kategorilist = new List<VwKategori>();
           CicekDBEntities kategori= new CicekDBEntities();

           kategorilist = kategori.VwKategori.ToList();

            dataGridView1.DataSource = kategorilist;
            this.Text = giris.KullaniciAdi + " " + giris.KullaniciSoyadi;
        }

        private void btnara_Click(object sender, EventArgs e)
        {
            List<VwKategori> ktglist = new List<VwKategori>();
            CicekDBEntities ktgr = new CicekDBEntities();

            ktglist = ktgr.VwKategori.Where(mrt => mrt.KategoriAdi.Contains(aratxt.Text)).ToList();
            dataGridView1.DataSource = ktglist;

        }

        private void button1_Click(object sender, EventArgs e)
        {

         
            
                Form3 yeni = new Form3();
                yeni.Show();


        }

        private void btngerifrm2_Click(object sender, EventArgs e)
        {
            Form1 yeni = new Form1();
            yeni.Show();
            this.Hide();
        }
    }
}
