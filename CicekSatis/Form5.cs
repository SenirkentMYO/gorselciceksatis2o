﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CicekSatis
{
    public partial class Form5 : Form
    {UrunSatis  urnsts;
        public Form5(UrunSatis u)
        {
            InitializeComponent();
            urnsts =u;
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            textBox1.Text = urnsts.Fiyati.ToString () ;
            textBox2.Text = urnsts.Adres;
            textBox3.Text = urnsts.TelefonNo;
        }

        private void btngerifrm5_Click(object sender, EventArgs e)
        {
            Form4 yeni = new Form4();
            yeni.Show();
            this.Hide();
        }
    }
}

