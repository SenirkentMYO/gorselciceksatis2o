﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CicekSatis
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btngiris_Click(object sender, EventArgs e)
        {
             List<Kullanici> kulanicilistesi = new List<Kullanici>();

            CicekDBEntities db = new CicekDBEntities();


            kulanicilistesi = db.Kullanici.Where(kulgir => kulgir.Email == txtemail .Text  && kulgir.Sifre == txtsifre .Text ).ToList();

            if (kulanicilistesi.Count == 1)
            {
                Form2 yeni = new Form2(kulanicilistesi [0]);
                yeni.Show();

                this.Hide();

            }
            else
            {
                MessageBox.Show("Yanlış Giriş Yaptınız Tekrar Deneyiniz");
            }
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        }
    }

