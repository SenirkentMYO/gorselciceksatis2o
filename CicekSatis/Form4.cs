﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CicekSatis
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            List<VwUrunSatis > satis = new List<VwUrunSatis >();
            CicekDBEntities db = new CicekDBEntities();

            satis = db.VwUrunSatis.ToList();

            dataGridView1.DataSource = satis;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            CicekDBEntities  db = new CicekDBEntities ();
            List<VwUrunSatis > liste = db.VwUrunSatis .ToList();
            var linqliste = (from s in liste
                             where s.Fiyati >= 75
                             select new
                             {
                                 fyt = s.Fiyati 

                             }).ToList();
            dataGridView1.DataSource = linqliste;
        }

        private void button2_Click(object sender, EventArgs e)
        {

            CicekDBEntities db = new CicekDBEntities();
            List<VwUrunSatis> liste = db.VwUrunSatis.ToList();
            var linqtoplam = (from s in liste
                             where s.Fiyati >= 12
                             select new
                             {
                                 fyt = s.Fiyati,
                                 urunıd = s.UrunID

                             }).Sum(a => a.fyt);
            MessageBox.Show(linqtoplam.ToString());
        }

        private void button3_Click(object sender, EventArgs e)
        {CicekDBEntities  db = new CicekDBEntities ();
                int secilenid = int.Parse(dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
            UrunSatis  u = db.UrunSatis .Where(a => a.ID == secilenid).First();

            Form5 f = new Form5(u);
            f.Show();


        }

        private void button4_Click(object sender, EventArgs e)
        {
            CicekDBEntities db = new CicekDBEntities();
            List<VwUrunSatis> liste = db.VwUrunSatis.ToList();
            var linqhepsi = (from s in liste
                              where s.Fiyati >= 200
                              select new
                              {
                                  fiyatt = s.Fiyati,
                                  adresi = s.Adres,
                                  numarası = s.TelefonNo
                              }).ToList();
            dataGridView1.DataSource = linqhepsi;
                                 
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form3 yeni = new Form3();
            yeni.Show();
            this.Hide();
        }
    }
}
