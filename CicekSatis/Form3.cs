﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CicekSatis
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CicekDBEntities db = new CicekDBEntities();
            List<VwUrun> urunn = new List<VwUrun>();
            urunn = db.VwUrun.ToList();

            var TListe = urunn.Select(a => new { UrunAdi = a.UrunAdi, UrunFiyat = a.UrunFiyat, KategoriAdi = a.KategoriAdi }).ToList();

            dataGridView1.DataSource = TListe;
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            List<VwUrun> urunlist = new List<VwUrun>();
            CicekDBEntities urun = new CicekDBEntities();

            urunlist = urun.VwUrun.ToList();

            dataGridView1.DataSource = urunlist;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CicekDBEntities db = new CicekDBEntities();
            List<VwUrun> liste = db.VwUrun.OrderBy(a => a.UrunFiyat).ToList();

            dataGridView1.DataSource = liste;







        }

        private void button3_Click(object sender, EventArgs e)
        {
            CicekDBEntities db = new CicekDBEntities();
            int secilenid = int.Parse(dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
            Urun  u = db.Urun.Where(a => a.ID == secilenid).First();

            Form6 f = new Form6(u);
            f.Show();

        }

        private void btngerifrm3_Click(object sender, EventArgs e)
        {

        }

        private void maxbtn_Click(object sender, EventArgs e)
        {
            CicekDBEntities db = new CicekDBEntities();
            List<VwUrun> liste = new List<VwUrun>();
            int enbuyuk = db.Urun.Max(a => a.ID);
            MessageBox.Show(enbuyuk.ToString());
        }

        private void uruntoplam_Click(object sender, EventArgs e)
        {
            CicekDBEntities db = new CicekDBEntities();
            List<VwUrun> liste = new List<VwUrun>();
            int toplam = db.Urun.Sum(a => a.ID);
            MessageBox.Show(toplam.ToString());
        }
    }
}

   