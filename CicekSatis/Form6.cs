﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CicekSatis
{
    public partial class Form6 : Form
    {
        Urun urunn;
        public Form6(Urun u)
        {
            InitializeComponent();
            urunn= u;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form4 yeni = new Form4();
            yeni.Show();


        }

        private void Form6_Load(object sender, EventArgs e)
        {
            textBox1.Text = urunn.UrunAdi .ToString();
            textBox2.Text = urunn.KategoriAdi;
            textBox3.Text = urunn.UrunFiyat.ToString () ;
        }
    }
}
