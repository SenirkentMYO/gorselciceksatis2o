﻿namespace CicekSatis
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.aratxt = new System.Windows.Forms.TextBox();
            this.btnara = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btngerifrm2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 54);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(643, 161);
            this.dataGridView1.TabIndex = 0;
            // 
            // aratxt
            // 
            this.aratxt.Location = new System.Drawing.Point(180, 12);
            this.aratxt.Name = "aratxt";
            this.aratxt.Size = new System.Drawing.Size(100, 20);
            this.aratxt.TabIndex = 1;
            // 
            // btnara
            // 
            this.btnara.Location = new System.Drawing.Point(64, 12);
            this.btnara.Name = "btnara";
            this.btnara.Size = new System.Drawing.Size(75, 23);
            this.btnara.TabIndex = 2;
            this.btnara.Text = "Ara";
            this.btnara.UseVisualStyleBackColor = true;
            this.btnara.Click += new System.EventHandler(this.btnara_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(535, 233);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Ürün Seçiniz";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btngerifrm2
            // 
            this.btngerifrm2.Location = new System.Drawing.Point(454, 234);
            this.btngerifrm2.Name = "btngerifrm2";
            this.btngerifrm2.Size = new System.Drawing.Size(75, 23);
            this.btngerifrm2.TabIndex = 4;
            this.btngerifrm2.Text = "Geri";
            this.btngerifrm2.UseVisualStyleBackColor = true;
            this.btngerifrm2.Click += new System.EventHandler(this.btngerifrm2_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(667, 277);
            this.Controls.Add(this.btngerifrm2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnara);
            this.Controls.Add(this.aratxt);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form2";
            this.Text = "Kategori Formu";
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox aratxt;
        private System.Windows.Forms.Button btnara;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btngerifrm2;
    }
}